Komet
=====

Dev requirements
----------------

- python (`>= 3.8 <https://python.org>`_)
- poetry (`Latest version <https://python-poetry.org>`_)

UNIX only requirements
----------------------

- libffi (`Latest version <https://github.com/libffi/libffi>`_)
- libnacl (`Latest version <https://github.com/saltstack/libnacl>`_)
- python3-dev (`Latest version <https://packages.debian.org/bullseye/python3-dev>`_)

How to run
----------

- Install the project dependencies

::

    poetry install

- Create a file named ".token" and it must only contain the token for the discord API

- Run the bot

::

    poetry run python komet
