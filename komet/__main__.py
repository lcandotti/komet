from pathlib import Path

from komet.client import bot

BASE_DIR = Path(__file__).parent.parent.resolve()


def main() -> None:
    fpath = BASE_DIR / ".token"

    if not fpath.exists():
        raise FileNotFoundError(f"Unable to locate .token file in {BASE_DIR}")

    with open(fpath, "r") as handler:
        token = handler.readline()

    bot.run(token)


if __name__ == "__main__":
    main()
