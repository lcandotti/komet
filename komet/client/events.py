from logging import getLogger

import discord

from komet.client.base import bot

discord.utils.setup_logging()

_log = getLogger(__name__)


@bot.event
async def on_ready() -> None:
    _log.info(f"Logged in as {bot.user} (ID: {bot.user.id})")


@bot.event
async def on_message(message: discord.Message) -> None:
    _log.info(f"Received message (ID {message.id}): \"{message.content}\" ({message.author.name}@{message.author.id})")
    await bot.process_commands(message)
