import discord
from discord.ext.commands import Bot


class KometBot(Bot):
    """"""

    def __init__(self, *args, **kwargs) -> None:
        command_prefix = kwargs.pop("command_prefix", "$")
        description = kwargs.pop("description", "KekW")

        intents = discord.Intents.default()
        intents.members = True
        intents.message_content = True

        super().__init__(
            command_prefix=command_prefix,
            description=description,
            intents=intents,
            *args,
            **kwargs,
        )


bot = KometBot()
