import random
from typing import List, Optional

import discord
from discord.ext.commands import Context

from komet.client.base import bot


@bot.command()
async def add(ctx: Context, left: int, right: int) -> None:
    """Adds two numbers together."""
    await ctx.send(left + right)  # type: ignore  # .send() accept more than str type


@bot.command()
async def choose(ctx: Context, *choices: List[str]) -> None:
    """Chooses between multiple choices."""
    await ctx.send(random.choice(choices))


@bot.command()
async def cool(ctx: Context, member: discord.Member) -> None:
    """Says if a user is cool."""
    if member.name != "Nexius":
        await ctx.send(f"No, {ctx.subcommand_passed} is not cool")
    else:
        await ctx.send(f"Yeah, {member.name} is pretty cool ! No cap !")


@bot.command()
async def hi(ctx: Context, member: discord.Member) -> None:
    """Say hi to someone"""
    await ctx.send(f"Hi, {member.name}")


@bot.command()
async def joined(ctx: Context, member: discord.Member) -> None:
    """Says when a member joined."""
    await ctx.send(f"{member.name} joined {discord.utils.format_dt(member.joined_at)}")


@bot.command()
async def repeat(ctx: Context, times: int, content: Optional[str] = None) -> None:
    """Repeats a message multiple times."""
    if content is None:
        content = "repeating ..."

    for i in range(times):
        await ctx.send(content)


@bot.command()
async def roll(ctx: Context, dice: str) -> None:
    """Rolls a dice in NdN format."""
    try:
        rolls, limit = map(int, dice.split("d"))
    except Exception:
        await ctx.send("Format has to be in NdN!")
        return

    result = ", ".join(str(random.randint(1, limit)) for r in range(rolls))
    await ctx.send(result)
